import "./css/__import";

// Configs
//////////////////////////////////////////////////
const isDevProxy = () => {
  try {
    const params = new URLSearchParams(window.location.search);
    if(params.get("dev-proxy") === "true") {
      return true
    }
    if((process.env.DEV_PROXY_ON || "").toLocaleLowerCase() === "true") {
      return true
    }
    return false
  } catch(err) {
    return false
  }
}

const sarnaDomain = isDevProxy()
  ? "http://localhost:8080"
  : "https://www.sarna.net";

const sarnaImageRootUrl = "https://cfw.sarna.net/wiki/images"

// Fetches
/////////////////////////////////////////////////
const fetchSarnaWikiPage = (pageName) =>
  fetch(`${sarnaDomain}/wiki/${pageName}`, { method: 'GET'})
    .then((response) => response.text())
    .catch((err) => console.warn("Error fetching Sarna page: " + err.Message))

// Utils
//////////////////////////////////////////////////
const parseText = (str) =>
  str.replace(/\[[1-9+]\]/gi, '').split(".").slice(0,7).join(".") + "."

const parseProfile = () =>
  Array.from(document.getElementById("profile").querySelectorAll('h2 ~ p')).map((el) => el.textContent).join()

const injectProfile = (profileText) => {
  document.getElementById("profile").innerHTML = profileText
  const corrected = parseText(parseProfile())
  document.getElementById("profile").innerHTML = corrected
}

const injectImage = (imagePath) => {
  document.getElementById("image").src = `${sarnaImageRootUrl}/${imagePath}`;
}

// Main
//////////////////////////////////////////////////
const main = () => {
  fetchSarnaWikiPage("Timber_Wolf_(Mad_Cat)")
    .then((profilePageData) => {
      const imagePath = "f/f5/Madcat_bt25yoaf_tro3050.jpg"; // Madcat_bt25yoaf_tro3050.jpg
    
      if(profilePageData) {      
        injectProfile(profilePageData)
        injectImage(imagePath)
      }

    });
}

window.addEventListener('DOMContentLoaded', main);
