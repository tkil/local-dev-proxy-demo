/// run-devServer.js

const proxy = require('http-proxy-middleware');
const Bundler = require('parcel-bundler');
const express = require('express');

const bundler = new Bundler('src/index.html', {
  cache: false,
});

const app = express();
const PROXY_PORT = 8080;
const LOCAL_DEV_PORT = 1234;

app.use(
  '/wiki/**', // Your pattern
  proxy({
    target: 'https://www.sarna.net',
    changeOrigin: true,
    secure: false,
    onProxyRes: proxyRes => {
      proxyRes.headers['Access-Control-Allow-Origin'] = `http://localhost:${LOCAL_DEV_PORT}` // Response header to white list localhost
    }
  })
);

// Pass the Parcel bundler into Express as middleware
app.use(bundler.middleware());

// Run your Express server
app.listen(PROXY_PORT);