# local-dev-proxy-demo
#### Repo: https://gitlab.com/tkil/local-dev-proxy-demo

### Summary
Simple demo app to illustrate how a local dev proxy works with webpack or local express

### Authors
* Tyler Kilburn <tylerkilburn@carfax.com>
